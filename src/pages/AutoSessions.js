import React from "react";
import Automatic from "../components/Automatic";
import purchase from "../static/purchase.png";
import { Tabs } from "antd";
import tenders from "../static/tenders.png";
import Session from "../components/Session";
const { TabPane } = Tabs;

const AutoSessions = () => {
  return (
    <div>
      <div>
        <img
          src={purchase}
          alt="purchase"
          style={{ width: "1200px", height: "280px", margin: "5px 10%" }}
        />
      </div>
      <Tabs
        defaultActiveKey="1"
        onChange={() => {}}
        style={{ width: "60%", margin: "5px 20%" }}
        size={"large"}
      >
        <TabPane tab="Сессии" key="1">
          <Session />
        </TabPane>
        <TabPane tab="Автоматизация" key="2">
          <Tabs
            defaultActiveKey="1"
            onChange={() => {}}
            style={{ width: "60%", margin: "5px 20%" }}
            size={"large"}
          >
            <TabPane tab="Пользователь 1" key="1">
              <Automatic userID="1" />
            </TabPane>
            <TabPane tab="Пользователь 2" key="2">
              <Automatic userID="2" />
            </TabPane>
          </Tabs>
        </TabPane>
      </Tabs>
    </div>
  );
};

export default AutoSessions;
