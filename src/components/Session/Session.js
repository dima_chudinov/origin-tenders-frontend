import React, { useEffect } from "react";
import {
  sessionApi,
  useGetTenderQuery,
  useGetBetsQuery,
} from "../../store/api/sessions";
import { useDispatch, useSelector } from "react-redux";
import CountDown from "./Countdown";

const Session = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state?.sessionsWS);
  console.log(state);
  const { data, isError, error } = useGetTenderQuery(
    "625bede31b45fde45c0876db",
    {
      pollingInterval: 3000,
    }
  );
  const bets = useGetBetsQuery("625bede31b45fde45c0876db", {
    pollingInterval: 3000,
  });
  // console.log(session, isError, error);
  useEffect(() => {
    async function connectWS() {
      // dispatch(sessionApi.endpoints.getBetsWS.initiate());
      // dispatch(sessionApi.endpoints.getSessionsInfo.initiate());
      // console.log(data);
      // console.log("Ws mounted");
    }
    connectWS();
    // const ws = new WebSocket("ws://10.10.117.179:8080/ws/bets/1")
    // try {
    //   // wait for the initial query to resolve before proceeding
    //   // await cacheDataLoaded;
    //   const listener = (event) => {
    //     // const data = JSON.parse(event.data);
    //     console.log(event?.data)
    //   };

    //   ws.addEventListener("message", listener);
    // } catch(e) {
    //   console.error(e)
    // }
  }, []);
  console.log(bets);
  return (
    <div>
      {isError ? <p>{error?.message}</p> : null}
      <div>
        <div>
          <div>Котировочная сессия {data?.id}</div>
          <p style={{ fontWeight: "bold", color: "green" }}>{data?.status}</p>
        </div>
        <div style={{ fontWeight: "bold", fontSize: "22px" }}>{data?.name}</div>
      </div>
      <div>
        <div>
          <div>До конца сессии осталось:</div>
          <div>
            <CountDown hours={1} minutes={45} />
          </div>
        </div>
        <div>
          <div>
            <p>Начальная цена:</p>
            <p>{data?.start_price} руб.</p>
          </div>
          {data?.current_price ? (
            <div>
              <p>Последняя цена:</p>
              <p>{data?.current_price} руб.</p>
            </div>
          ) : null}
        </div>
        <div>
          <div>
            {bets?.data?.length ? (
              <>
                <p>Ставки:</p>
                {bets?.data?.map((bet) => (
                  <p>{bet?.id}</p>
                ))}
              </>
            ) : (
              <p>Ставки остустствуют!</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Session;
