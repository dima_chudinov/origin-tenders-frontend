import React from "react";
import { Switch, Tabs } from "antd";
import Notifications from "./Notifications";
import Tenders from "./Tenders";
import Analytics from "./Analytics";
import Settings from "./Settings";
const { TabPane } = Tabs;

const Automatic = () => {
  return (
    <div>
      <div style={{ width: "500px", fontSize: "18px" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p>Автоматическое участие: </p>
          <p style={{ paddingLeft: "10px" }}>
            <Switch defaultChecked />
          </p>
        </div>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p>Подтверждения действий бота: </p>
          <p style={{ paddingLeft: "10px" }}>
            <Switch />
          </p>
        </div>
      </div>
      <Tabs defaultActiveKey="1" onChange={() => {}} size={"large"}>
        <TabPane tab="Уведомления" key="1" style={{ fontSize: "22px" }}>
          <Notifications />
        </TabPane>
        <TabPane tab="Тендеры" key="2" style={{ fontSize: "22px" }}>
          <Tenders />
        </TabPane>
        <TabPane tab="Аналитика" key="3" style={{ fontSize: "22px" }}>
          <Analytics />
        </TabPane>
        <TabPane tab="Настройки" key="4" style={{ fontSize: "22px" }}>
          <Settings />
        </TabPane>
      </Tabs>
    </div>
  );
};

export default Automatic;
