import React, { useState } from "react";
import { Button, DatePicker, Switch } from "antd";
import moment from "moment";

const dateFormat = "YYYY/MM/DD";
const Filter = () => {
  let dt = new Date();
  dt.setDate(dt.getDate() - 30);
  const [dateStart, setDateStart] = useState(dt);
  const [dateEnd, setDateEnd] = useState(new Date());
  return (
    <div>
      <div style={{ display: "flex" }}>
        <p style={{ marginRight: "20px" }}>Стандартная аналитика</p>
        <div>
          <Switch />
        </div>
        <p style={{ marginLeft: "20px" }}>Расширенная аналитика</p>
      </div>
      <div style={{ display: "flex", justifyContent: "space-around" }}>
        <div style={{ display: "flex", alignItems: "center" }}>
          <p style={{ margin: "0px", padding: "0" }}>Дата начала:</p>
          <DatePicker
            defaultValue={moment(dateStart, dateFormat)}
            onChange={() => {}}
            style={{ marginLeft: "20px" }}
          />
        </div>
        <div style={{ display: "flex", alignItems: "center" }}>
          <p style={{ margin: "0px", padding: "0" }}>Дата окончания:</p>
          <DatePicker
            defaultValue={moment(dateEnd, dateFormat)}
            onChange={() => {}}
            style={{ marginLeft: "20px" }}
          />
        </div>
        <div>
          <Button type="primary">Применить</Button>
        </div>
      </div>
    </div>
  );
};

export default Filter;
