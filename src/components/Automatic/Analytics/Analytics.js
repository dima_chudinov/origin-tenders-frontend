import React from "react";
import BaseStatistics from "./BaseStatistics";
import Filter from "./Filter";
import Tenders from "./Tenders";

const Analytics = () => {
  return (
    <div>
      <Filter />
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          margin: "20px 5px",
        }}
      >
        <div style={{ width: "300px", height: "300px" }}>
          <BaseStatistics />
        </div>
        <p style={{ marginTop: "20px", fontSize: "32px" }}>
          Процент побед в сессиях:
          <span style={{ marginLeft: "10px", fontWeight: "bold" }}>40%</span>
        </p>
      </div>
      <div>
        <div style={{ width: "100%",  }}>
          <Tenders />
        </div>
      </div>
    </div>
  );
};

export default Analytics;
