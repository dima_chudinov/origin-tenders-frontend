import React from "react";
import { Pie } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";

export const data = {
  labels: ["Проигранные сессии", "Выйгранные сессии"],
  datasets: [
    {
      label: "# of Votes",
      data: [19, 12],
      backgroundColor: ["rgba(255, 99, 132, 0.5)", "rgba(53, 162, 235, 0.5)"],
      // borderColor: ["rgba(153, 102, 255, 1)", "rgba(255, 159, 64, 1)"],
      borderWidth: 1,
    },
  ],
};

const BaseStatistics = () => {
  ChartJS.register(ArcElement, Tooltip, Legend);
  return <Pie data={data} />;
};

export default BaseStatistics;
