import { Button, Select } from "antd";
import React from "react";
import Filter from "./Filter";

const { Option } = Select;

const Settings = () => {
  return (
    <div>
      <div style={{ display: "flex" }}>
        {/* <div style={{ display: "flex", justifyContent: "space-around" }}>
          <p>Фильтры</p>
          <p>
            <Button type="primary">Создать новый фильтр</Button>
          </p>
        </div>
        <div style={{marginLeft:"25px"}}>
          <Filter />
        </div> */}
        <p>Стратегия:</p>
        <Select defaultValue={"to_small"} style={{ width: "200px" }}>
          <Option value="to_small">К минимальной цене</Option>
          <Option value="race_win">Победа любой ценой</Option>
          <Option value="curr_procent">Процент от прибыли</Option>
        </Select>
      </div>
      <div></div>
      <p>
        <Button type="primary">Сохранить</Button>
      </p>
    </div>
  );
};

export default Settings;
