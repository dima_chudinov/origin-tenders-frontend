import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchTenders } from "../../../store/feautures/tendersSlice";
import { Alert } from "antd";
import Tender from "./Tender";

const Tenders = () => {
  const dispatch = useDispatch();
  const tenders = useSelector((state) => state.tenders);
  useEffect(() => {
    // dispatch(fetchTenders(1));
  }, []);
  return (
    <div>
      {tenders.errorMessage ?? (
        <Alert type={"error"} message={tenders.errorMessage} />
      )}
      {tenders?.tenders?.map((tender) => (
        // <p key={tender._id}>{tender.name}</p>
        <div style={{ marginTop: "10px" }}>
          <Tender
            key={tender._id}
            name={tender.name}
            shortDescription={tender.short_description}
            description={tender.description}
          />
        </div>
      ))}
    </div>
  );
};

export default Tenders;
