import { Button, Card, Switch } from "antd";
import React, { useState } from "react";
const tabList = [
  {
    key: "info",
    tab: "Информация",
  },
  {
    key: "tab2",
    tab: "Настройки",
  },
];

const Tender = (props) => {
  const [activeTabKey1, setActiveTabKey1] = useState("info");
  const [isOpen, setisOpen] = useState(false);
  const contentList = {
    tab1: (
      <div>
        {isOpen ? (
            <div>{props.description}</div>
        ) : null}
      </div>
    ),
    tab2: (
      <div>
        <div style={{ display: "flex" }}>
          <p>Автоматическое участие</p>
          <p style={{ marginLeft: "20px" }}>
            <Switch defaultChecked />
          </p>
        </div>
      </div>
    ),
  };

  return (
    <Card
      title={props.name}
      bordered={true}
      size={"large"}
      activeTabKey={activeTabKey1}
      tabList={isOpen ? tabList : []}
      onTabChange={(key) => {
        setActiveTabKey1(key);
      }}
      extra={
        <Button
          danger={isOpen}
          type="primary"
          onClick={() => setisOpen(!isOpen)}
        >
          Подробности
        </Button>
      }
    >
      {contentList[activeTabKey1]}
      {activeTabKey1 === "info" ? <div>{props.shortDescription}</div> : null}
    </Card>
  );
};

export default Tender;
