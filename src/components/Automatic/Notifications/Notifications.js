import React from "react";
import { useSelector } from "react-redux";
import Notification from "./Notification";

const Notifications = () => {
  const notifications = useSelector((state) => state.notifications);
  console.log(notifications);
  return (
    <div>
      {notifications?.notifications?.length ? (
        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            fontWeight: "bold",
          }}
        >
          <p>Описание</p>
          <p>Действие</p>
        </div>
      ) : null}
      {notifications?.notifications?.map((notification) => (
        <Notification
          key={notification.id}
          description={notification.description}
          name={notification.description}
          id={notification.id}
          isNeedApprove={notification?.isNeedApprove}
          isApproved={notification?.isApproved}
          isApprove={notification?.isApprove}
        />
      ))}
    </div>
  );
};

export default Notifications;
