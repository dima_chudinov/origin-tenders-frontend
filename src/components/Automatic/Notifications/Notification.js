import { Button, Card } from "antd";
import React, { useState } from "react";

const Notification = (props) => {
  const [isOpen, setisOpen] = useState(false);
  return (
    <Card
      title={props.name}
      bordered={true}
      size={"large"}
      extra={
        <div style={{ display: "flex" }}>
          {props?.isNeedApprove ? (
            props?.isApproved ? (
              <p>
                {props.isApprove ? (
                  <Button type="primary" style={{ margin: "0 5px" }} >Подтверждено</Button>
                ) : (
                  <Button danger style={{ margin: "0 5px" }} >
                    Отклонено
                  </Button>
                )}
              </p>
            ) : (
              <div style={{ display: "flex" }}>
                <Button type="primary">Подтвердить</Button>
                <Button danger style={{ marginLeft: "15px" }}>
                  Отклонить
                </Button>
              </div>
            )
          ) : null}
          <Button type="text" onClick={() => setisOpen(!isOpen)}>
            Подробности
          </Button>
        </div>
      }
    >
      <div>Краткая справка по действию бота</div>
      {isOpen ? (
        <>
          <div>fhuushaduihd suid uashd iuas hd hs sdhuiahshd </div>
          <div>fhuushaduihd suid uashd iuas hd hs sdhuiahshd </div>
          <div>fhuushaduihd suid uashd iuas hd hs sdhuiahshd </div>
          <div>fhuushaduihd suid uashd iuas hd hs sdhuiahshd </div>
        </>
      ) : null}
    </Card>
  );
};

export default Notification;

// import React from "react";
// import { Button } from "antd";
// const Notification = (props) => {
//   return (
//     <div style={{display:"flex", justifyContent:"space-between"}}>
//       <p>{props.description}</p>
//       {props?.isNeedApprove ? (
//         props?.isApproved ? (
//           <p>{props.isApprove ? "Подтверждено" : "Отклонено"}</p>
//         ) : (
//           <div style={{display:"flex"}}>
//             <Button type="primary">Подтвердить</Button>
//             <Button danger style={{marginLeft:"15px"}}>Отклонить</Button>
//           </div>
//         )
//       ) : null}
//     </div>
//   );
// };

// export default Notification;
