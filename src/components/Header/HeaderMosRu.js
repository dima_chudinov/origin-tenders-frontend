import React from "react";
import ppLogo from "../../static/header/pp_logo.svg"

const Header = () => {
  return (
    <div className="TopMenuStyles__TopMenuContainer-sc-1jpq1td-0 fCIduM">
      <div className="ui container">
        <div className="ui vertically padded middle aligned grid">
          <div className="row">
            <div className="three wide computer three wide large screen four wide mobile four wide tablet three wide widescreen column TopMenuStyles__FlexColumn-sc-1jpq1td-13 CtCTg">
              <div style="display: inline-block">
                <a href="https://zakupki.mos.ru/ru">
                  <img
                    src={ppLogo}
                    className="ui inline image TopMenuStyles__FullLogo-sc-1jpq1td-1 lovIDC"
                    height="38"
                  />
                </a>
              </div>
              <div className="MainMenuStyles__MainMenuButtonContainer-sc-1dp6bew-0 jwTKdm">
                <div
                  style="display: inline-block"
                  className="ui small header MainMenuButtonStyles__MenuButtonHeader-sc-e72y7r-1 isNiHF"
                >
                  <div className="MainMenuButtonStyles__BurgerMenu-sc-e72y7r-3 cfRmlX">
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>
                  <div className="content MainMenuButtonStyles__MenuButtonCaption-sc-e72y7r-2 bCAqKr">
                    Меню
                  </div>
                </div>
                <div
                  style="display: none"
                  className="ui small header MainMenuButtonStyles__MenuButtonHeader-sc-e72y7r-1 isNiHF"
                >
                  <img
                    src="%D0%A0%D0%B5%D0%B5%D1%81%D1%82%D1%80%20%D0%B7%D0%B0%D0%BA%D1%83%D0%BF%D0%BE%D0%BA%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2,%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82,%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%20%D0%BD%D0%B0%20%D0%9F%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D0%B5%20%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D1%89%D0%B8%D0%BA%D0%BE%D0%B2_files/close.svg"
                    className="ui image MainMenuButtonStyles__MenuIconImage-sc-e72y7r-0 bvkWPJ"
                  />
                  <div className="content MainMenuButtonStyles__MenuButtonCaption-sc-e72y7r-2 bCAqKr">
                    Меню
                  </div>
                </div>
              </div>
            </div>
            <div className="center aligned one wide computer five wide large screen one wide mobile one wide tablet five wide widescreen column">
              <div className="SupportModalStyles__Trigger-sc-1th3c7c-0 cYXwfl">
                <div className="ui small header TopMenuStyles__SupportLink-sc-1jpq1td-8 hNySIw">
                  <img
                    src="%D0%A0%D0%B5%D0%B5%D1%81%D1%82%D1%80%20%D0%B7%D0%B0%D0%BA%D1%83%D0%BF%D0%BE%D0%BA%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2,%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82,%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%20%D0%BD%D0%B0%20%D0%9F%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D0%B5%20%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D1%89%D0%B8%D0%BA%D0%BE%D0%B2_files/qa.svg"
                    className="ui image TopMenuStyles__SupportImg-sc-1jpq1td-6 eSwSgB"
                  />
                  <div className="content">Оставить обращение</div>
                </div>
              </div>
              <a href="https://zakupki.mos.ru/knowledgebase/main">
                <div className="ui small header TopMenuStyles__SupportLink-sc-1jpq1td-8 TopMenuStyles__QALink-sc-1jpq1td-9 hNySIw eDPhQi">
                  <img
                    src="%D0%A0%D0%B5%D0%B5%D1%81%D1%82%D1%80%20%D0%B7%D0%B0%D0%BA%D1%83%D0%BF%D0%BE%D0%BA%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2,%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82,%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%20%D0%BD%D0%B0%20%D0%9F%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D0%B5%20%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D1%89%D0%B8%D0%BA%D0%BE%D0%B2_files/idea.svg"
                    className="ui image TopMenuStyles__KnowledgeBaseImg-sc-1jpq1td-7 hUsHIc"
                  />
                  <div className="content">Центр поддержки</div>
                </div>
              </a>
              <span className="SpanLink-sc-18ruhpr-0 TopMenuStyles__RegionSelectorSpan-sc-1jpq1td-10 gPpPWg ckMNLv">
                <span>
                  <span>
                    <div className="ui small header RegionSelectorPopupStyles__RegionSelectorLink-sc-34djio-0 bfwdBt">
                      <div className="ui image RegionSelectorPopupStyles__RegionSelectorImg-sc-34djio-1 bIBwSn">
                        <img
                          src="%D0%A0%D0%B5%D0%B5%D1%81%D1%82%D1%80%20%D0%B7%D0%B0%D0%BA%D1%83%D0%BF%D0%BE%D0%BA%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2,%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82,%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%20%D0%BD%D0%B0%20%D0%9F%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D0%B5%20%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D1%89%D0%B8%D0%BA%D0%BE%D0%B2_files/direction.svg"
                          className="ui image"
                        />
                      </div>
                      <div className="content RegionSelectorPopupStyles__RegionNameContainer-sc-34djio-3 cDDgsb">
                        <div
                          title="г Москва"
                          className="content RegionSelectorPopupStyles__RegionNameContainer-sc-34djio-3 RegionSelectorPopupStyles__RegionNameContainerGradient-sc-34djio-4 cDDgsb jiDmZm"
                        >
                          г Москва
                        </div>
                      </div>
                    </div>
                  </span>
                </span>
              </span>
            </div>
            <div
              style="justify-content: flex-end"
              className="right aligned right floated eight wide computer four wide large screen seven wide mobile seven wide tablet four wide widescreen column TopMenuStyles__FlexColumn-sc-1jpq1td-13 CtCTg"
            >
              <span className="SpanLink-sc-18ruhpr-0 gPpPWg">
                <span>
                  <div className="CartPopupButtonStyles__CartLink-sc-1ndjsp1-0 NlbPH ui small header">
                    <div className="CartPopupButtonStyles__CartImage-sc-1ndjsp1-1 ilcIYB">
                      <img
                        src="%D0%A0%D0%B5%D0%B5%D1%81%D1%82%D1%80%20%D0%B7%D0%B0%D0%BA%D1%83%D0%BF%D0%BE%D0%BA%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2,%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82,%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%20%D0%BD%D0%B0%20%D0%9F%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D0%B5%20%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D1%89%D0%B8%D0%BA%D0%BE%D0%B2_files/cart-active.svg"
                        className="ui hidden image"
                      />
                      <img
                        src="%D0%A0%D0%B5%D0%B5%D1%81%D1%82%D1%80%20%D0%B7%D0%B0%D0%BA%D1%83%D0%BF%D0%BE%D0%BA%20%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2,%20%D1%80%D0%B0%D0%B1%D0%BE%D1%82,%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%20%D0%BD%D0%B0%20%D0%9F%D0%BE%D1%80%D1%82%D0%B0%D0%BB%D0%B5%20%D0%BF%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D1%89%D0%B8%D0%BA%D0%BE%D0%B2_files/cart.svg"
                        className="ui image"
                      />
                    </div>
                  </div>
                </span>
              </span>
              <div className="ui right floated buttons">
                <button className="ui button">Войти</button>
                <a
                  type="button"
                  className="ui primary button LightButton-sc-1aa6yvl-0 LightButton__PrimaryButton-sc-1aa6yvl-2 bPAVwy QPlAK"
                  role="button"
                  href="https://zakupki.mos.ru/registration"
                >
                  <span>Зарегистрироваться</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
