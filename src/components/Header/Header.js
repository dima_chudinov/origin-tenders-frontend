import React from "react";
import ppLogo from "../../static/header/pp_logo.svg";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        marginTop: "20px",
      }}
    >
      <img src={ppLogo} alt="logo" />
      <p style={{fontSize:"24px", textAlign:"center"}}>
        <Link to={"/"}>Реестр закупок</Link>
      </p>
      <p style={{fontSize:"28px", textAlign:"center", fontWeight:"bold", border:"2px solid red", padding:"5px 10px"}}>
        <Link to={"/settings/user"}>Дмитрий Чудинов</Link>
      </p>
    </div>
  );
};

export default Header;
