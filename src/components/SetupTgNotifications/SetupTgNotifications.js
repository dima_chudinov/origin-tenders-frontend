import React, { useState } from "react";
import { Switch, Input, Button, Alert } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchApproveUserNick,
  fetchApproveGroupID,
} from "../../store/feautures/userSettingsSlice";
import { save } from "../../store/feautures/userSettingsSlice";

const SetupTgNotifications = () => {
  const [isDisplayLogin, setIsDisplayLogin] = useState(false);
  const [isDisplayWorkGroup, setIsDisplayWorkGroup] = useState(false);
  const [nickTg, setNickTg] = useState("");
  const [groupIDTg, setGroupIDTg] = useState("");
  const settings = useSelector((state) => state.userSettings);
  console.log(settings);
  const dispatch = useDispatch();
  return (
    <div>
      <div style={{ width: "700px", margin: "0 auto" }}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <p style={{ fontSize: "22px" }}>
            Уведомления в телеграм (в личные сообщения):
          </p>
          <Switch
            defaultChecked={isDisplayLogin}
            onChange={() => {
              setIsDisplayLogin(!isDisplayLogin);
            }}
          />
        </div>
        <div
          style={{
            display: `${isDisplayLogin ? "flex" : "none"}`,
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <p style={{fontSize: "22px"}}>Введите логин в телеграм:</p>
          <div style={{ display: "flex", alignItems: "center" }}>
            <p>
              <Input
                placeholder="Логин в телеграм"
                onChange={(e) => {
                  setNickTg(e.target.value);
                }}
              />
            </p>
            <p>
              <Button
                type="primary"
                disabled={!(nickTg?.length > 3)}
                onClick={() => {
                  dispatch(fetchApproveUserNick(nickTg));
                  setTimeout(() => {
                    dispatch(save());
                  }, 20000);
                }}
                style={{ marginLeft: "10px", fontSize: "14px" }}
              >
                Применить
              </Button>
            </p>
          </div>
        </div>
        {settings?.approveUserToken?.length ? (
          <Alert
            message={"Введите токен боту: " + settings?.approveUserToken}
            type="info"
            style={{fontSize:"24px"}}
          />
        ) : null}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <p style={{fontSize: "22px"}}>Уведомления в телеграм (в рабочую группу):</p>
          <Switch
            defaultChecked={isDisplayWorkGroup}
            onChange={() => {
              setIsDisplayWorkGroup(!isDisplayWorkGroup);
              setTimeout(() => {
                dispatch(save());
              }, 20000);
            }}
          />
        </div>
        <div
          style={{
            display: `${isDisplayWorkGroup ? "flex" : "none"}`,
            justifyContent: "space-between",
            width: "100%",
          }}
        >
          <p style={{fontSize: "18px"}}>Введите ID рабочей группы в телеграм:</p>
          <p style={{ paddingLeft: "15px" }}>
            <Input
              placeholder="ID рабочей группы"
              onChange={(e) => setGroupIDTg(e.target.value)}
            />
          </p>
          <p>
            <Button
              type="primary"
              disabled={!(groupIDTg?.length > 3)}
              onClick={() => {
                dispatch(fetchApproveGroupID(groupIDTg));
              }}
            >
              Применить
            </Button>
          </p>
        </div>
        {settings?.approveGroupToken?.length ? (
          <Alert
            message={"Введите токен боту: " + settings?.approveGroupToken}
            type="info"
          />
        ) : null}
      </div>
      <p style={{ textAlign: "center" }}>
        <Button
          type="primary"
          onClick={() => {
            dispatch(save());
          }}
        >
          Сохранить
        </Button>
      </p>
    </div>
  );
};

export default SetupTgNotifications;
