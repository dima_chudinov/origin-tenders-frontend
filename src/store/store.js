import { configureStore } from "@reduxjs/toolkit";
import reducers from "./feautures";
import { sessionApi } from "./api/sessions";

export const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(sessionApi.middleware),
});
