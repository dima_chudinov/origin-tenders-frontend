import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../api/axios";

export const fetchTenders = createAsyncThunk("automatic/tenders", (page) =>
  axios
    .get(`/tenders`)
    .then((response) => response.data)
    .catch((error) => error)
);

const initialState = {
  bets: [],
  tenders: [
    {
      _id: "1",
      timeEnd: "2022-04-17T16:05:49.73609064Z",
      name: "Покупка разеток тип С",
      description:
        "Розетки дложны соответсвовать стандарту. Закупка производится от 600 тыс. едениц товара",
      short_description: "Розетки должны быть типа С - европейский стандарт.",
      Filters: ["Фильтр по цене"],
      startPrice: 123123120,
      currentPrice: 1.11,
      status: "Активна",
      StepPercent: 0.5,
    },
    {
      _id: "2",
      timeEnd: "2022-04-17T16:05:49.73609064Z",
      name: "Покупка разеток тип С",
      description:
        "Розетки дложны соответсвовать стандарту. Закупка производится от 600 тыс. едениц товара",
      short_description: "Розетки должны быть типа С - европейский стандарт.",
      Filters: ["Фильтр по цене"],
      startPrice: 123123120,
      currentPrice: 1.11,
      status: "Активна",
      StepPercent: 0.5,
    },
    {
      _id: "3",
      timeEnd: "2022-04-17T16:05:49.73609064Z",
      name: "Покупка разеток тип С",
      description:
        "Розетки дложны соответсвовать стандарту. Закупка производится от 600 тыс. едениц товара",
      short_description: "Розетки должны быть типа С - европейский стандарт.",
      Filters: ["Фильтр по цене"],
      startPrice: 123123120,
      currentPrice: 1.11,
      status: "Активна",
      StepPercent: 0.5,
    },
    {
      _id: "4",
      timeEnd: "2022-04-17T16:05:49.73609064Z",
      name: "Покупка разеток тип С",
      description:
        "Розетки дложны соответсвовать стандарту. Закупка производится от 600 тыс. едениц товара",
      short_description: "Розетки должны быть типа С - европейский стандарт.",
      Filters: ["Фильтр по цене"],
      startPrice: 123123120,
      currentPrice: 1.11,
      status: "Активна",
      StepPercent: 0.5,
    },
  ],
  isLoading: false,
  page: 1,
  errorMessage: "",
};

export const tendersSlice = createSlice({
  name: "tenders",
  initialState,
  reducers: {
    addBetToTender: (state, action) => {
      console.log("add tender")
      state.bets.shift(action.payload);
    },
    replaceBets: (state, action) => {
      state.bets = action.payload;
    },
  },
  extraReducers: (builder) => {
    // Tenders
    builder.addCase(fetchTenders.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchTenders.fulfilled, (state, action) => {
      state.isLoading = false;
      state.tenders = action.payload.tenders;
      state.page = action.payload.page;
    });
    builder.addCase(fetchTenders.rejected, (state) => {
      state.isLoading = false;
      state.errorMessage = "Произошла ошибка, повторите попытку позже";
    });
  },
});

// Action creators are generated for each case reducer function
export const { addBetToTender, replaceBets } = tendersSlice.actions;

export default tendersSlice.reducer;
