import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../api/axios";

export const fetchAnalytics = createAsyncThunk(
  "automatic/analytics",
  (page) =>
    axios
      .get(``)
      .then((response) => response.data)
      .catch((error) => error)
);

const initialState = {
  all: 3539,
  wins: 1297,
  lose: 2242,
  winratePercent: 52,
  qtyBets: 6367776,
  isLoading: false,
  page: 1,
  errorMessage: "",
};

export const analyticsSlice = createSlice({
  name: "analytics",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Notifications
    builder.addCase(fetchAnalytics.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchAnalytics.fulfilled, (state, action) => {
      state.isLoading = false;
    });
    builder.addCase(fetchAnalytics.rejected, (state) => {
      state.isLoading = false;
      state.errorMessage = "Произошла ошибка, повторите попытку позже";
    });
  },
});

// Action creators are generated for each case reducer function
export const {} = analyticsSlice.actions;

export default analyticsSlice.reducer;
