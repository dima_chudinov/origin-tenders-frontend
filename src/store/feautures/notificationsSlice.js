import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../api/axios";

export const fetchNotifications = createAsyncThunk(
  "automatic/notifications",
  (page) =>
    axios
      .get(``)
      .then((response) => response.data)
      .catch((error) => error)
);

const initialState = {
  notifications: [
    {
      id: "1",
      type: "participation",
      description: "Бот сделал ставку в тендере ХХХ",
      tenderID: "1",
      price: 2212,
      isNeedApprove: false,
      isApprove: true,
    },
    {
      id: "2",
      type: "participation",
      description: "Бот рекомендует сделать ставку в тендере ХХХ",
      tenderID: "2",
      price: 2212,
      isNeedApprove: true,
      isApproved: false,
      isApprove: false,
    },
    {
      id: "3",
      type: "participation",
      description: "Бот сделал ставку в тендере ХХХ",
      tenderID: "3",
      price: 2212,
      isNeedApprove: true,
      isApproved: true,
      isApprove: false,
    },
    {
      id: "4",
      type: "participation",
      description: "Бот сделал ставку в тендере ХХХ",
      tenderID: "4",
      price: 2212,
      isNeedApprove: true,
      isApproved: true,
      isApprove: true,
    },
    {
      id: "5",
      type: "participation",
      description: "Бот сделал ставку в тендере ХХХ",
      tenderID: "5",
      price: 2212,
      isNeedApprove: false,
      isApprove: false,
    },
  ],
  isLoading: false,
  page: 1,
  errorMessage: "",
};

export const notificationsSlice = createSlice({
  name: "notifications",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    // Notifications
    builder.addCase(fetchNotifications.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchNotifications.fulfilled, (state, action) => {
      state.isLoading = false;
      state.notifications = action.payload.notifications;
      state.page = action.payload.page;
    });
    builder.addCase(fetchNotifications.rejected, (state) => {
      state.isLoading = false;
      state.errorMessage = "Произошла ошибка, повторите попытку позже";
    });
  },
});

// Action creators are generated for each case reducer function
// export const {} = notificationsSlice.actions;

export default notificationsSlice.reducer;
