import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "../../api/axios";

export const fetchApproveUserNick = createAsyncThunk(
  "user_settings/approveUserNick",
  (userNick) =>
    axios
      .post(`/bot/generateToken2`, {
        type: "user",
        value: userNick,
      })
      .then((response) => response.data)
      .catch((error) => error)
);

export const fetchApproveGroupID = createAsyncThunk(
  "user_settings/approveGroupID",
  (groupID) =>
    axios
      .post(`/bot/generateToken2`, {
        type: "user",
        value: groupID,
      })
      .then((response) => response.data)
      .catch((error) => error)
);

export const fetchUserSettings = createAsyncThunk(
  "user_settings/fetchInfo",
  () =>
    axios
      .get(``)
      .then((response) => response.data)
      .catch((error) => error)
);

const initialState = {
  isSendTelegramNotificationsToUser: false,
  isSendTelegramNotificationsToGroup: false,
  userTgNick: "",
  approveUserToken: "",
  groupTgID: "",
  approveGroupToken: "",
  isLoading: false,
  errorMessage: "",
};

export const userSettingsSlice = createSlice({
  name: "user_settings",
  initialState,
  reducers: {
    sendTelegramNotificationsToUser: (state, action) => {
      state.isSendTelegramNotificationsToUser = action.payload.isSend;
      state.userTgNick = action.payload.userTgNick;
    },
    sendTelegramNotificationsToGroup: (state, action) => {
      state.isSendTelegramNotificationsToGroup = action.payload.isSend;
      state.groupTgID = action.payload.groupTgID;
    },
    clearErrorMessage: (state) => {
      state.errorMessage = "";
    },
    save: (state) => {
      state.errorMessage = "";
      state.approveGroupToken = "";
      state.approveUserToken = "";
    },
  },
  extraReducers: (builder) => {
    // Approve UserNick
    builder.addCase(fetchApproveUserNick.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchApproveUserNick.fulfilled, (state, action) => {
      state.isLoading = false;
      state.approveUserToken = action.payload.token;
    });
    builder.addCase(fetchApproveUserNick.rejected, (state) => {
      state.isLoading = false;
      state.errorMessage = "Произошла ошибка, повторите попытку позже";
    });
    // Approve GroupID
    builder.addCase(fetchApproveGroupID.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchApproveGroupID.fulfilled, (state, action) => {
      state.isLoading = false;
      state.approveGroupToken = action.payload.token;
    });
    builder.addCase(fetchApproveGroupID.rejected, (state) => {
      state.isLoading = false;
      state.errorMessage = "Произошла ошибка, повторите попытку позже";
    });
    // Info
    builder.addCase(fetchUserSettings.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchUserSettings.fulfilled, (state, action) => {
      state.isLoading = false;
      // state.isSendTelegramNotificationsToUser =
      // state.userTgNick = action.payload.token;

      // state.isSendTelegramNotificationsToGroup =
      // state.groupTgID = action.payload.token;
    });
    builder.addCase(fetchUserSettings.rejected, (state) => {
      state.isLoading = false;
      state.errorMessage = "Произошла ошибка, повторите попытку позже";
    });
  },
});

export const {
  sendTelegramNotificationsToUser,
  sendTelegramNotificationsToGroup,
  save,
} = userSettingsSlice.actions;

export default userSettingsSlice.reducer;
