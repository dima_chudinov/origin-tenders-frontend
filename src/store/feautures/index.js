import { combineReducers } from "redux";
import userSettings from "./userSettingsSlice";
import notifications from "./notificationsSlice";
import tenders from "./tendersSlice";
import { sessionApi } from "../api/sessions";

export default combineReducers({
  userSettings,
  notifications,
  tenders,
  [sessionApi.reducerPath]: sessionApi.reducer,
});
