import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import {addBetToTender} from "../feautures/tendersSlice"
const mainWSUrl = "ws://10.10.117.179:8080/ws";

export const sessionApi = createApi({
  reducerPath: "sessionsWS",
  baseQuery: fetchBaseQuery({ baseUrl: "http://10.10.117.179:8080/" }),
  endpoints: (build) => ({
    getTender: build.query({
      query: (id) => `tender/${id}`,
    }),
    getBets: build.query({
      query: (id) => `orders/${id}`,
    }),
    getBetsWS: build.query({
      query: () => `bets`,
      async onCacheEntryAdded(
        arg,
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved }
      ) {
        const mockUserID = "1";
        const ws = new WebSocket(mainWSUrl + "/bets/" + mockUserID);
        try {
          // wait for the initial query to resolve before proceeding
          // await cacheDataLoaded;
          const listener = (event) => {
            console.log(event?.data)
            const data = JSON.parse(event.data);
            addBetToTender(data)
            console.log("receive", data);
            if (data.channel !== arg) return;

            updateCachedData((draft) => {
              console.log("Data updated", data);
              console.log(draft)
              draft.push(data);
            });
          };

          ws.addEventListener("message", listener);
        } catch(e) {
          console.error(e)
        }
        // await cacheEntryRemoved;
        ws.close();
      },
    }),
    getNotifications: build.query({
      query: (userID) => `notifications/` + userID,
      async onCacheEntryAdded(
        arg,
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved }
      ) {
        console.log(arg);
        const ws = new WebSocket(mainWSUrl + "/notifications");
        try {
          // wait for the initial query to resolve before proceeding
          await cacheDataLoaded;
          const listener = (event) => {
            const data = JSON.parse(event.data);
            console.log("receive", data);
            if (data.channel !== arg) return;

            updateCachedData((draft) => {
              console.log("Data updated");
              draft.push(data);
            });
          };

          ws.addEventListener("message", listener);
        } catch {}
        await cacheEntryRemoved;
        ws.close();
      },
    }),
    getSessionsInfo: build.query({
      query: () => `session`,
      async onCacheEntryAdded(
        arg,
        { updateCachedData, cacheDataLoaded, cacheEntryRemoved }
      ) {
        console.log(arg);
        const mockUserID = "1";
        const ws = new WebSocket(mainWSUrl + "/session/" + mockUserID);
        try {
          // wait for the initial query to resolve before proceeding
          await cacheDataLoaded;
          const listener = (event) => {
            const data = JSON.parse(event.data);
            console.log("receive", data);
            if (data.channel !== arg) return;

            updateCachedData((draft) => {
              console.log("Data updated");
              draft.push(data);
            });
          };

          ws.addEventListener("message", listener);
        } catch {}
        await cacheEntryRemoved;
        ws.close();
      },
    }),
  }),
});

export const {
  useGetBetsQuery,
  useGetNotificationsQuery,
  useGetTenderQuery,
} = sessionApi;
