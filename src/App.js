// import logo from './logo.svg';
import { Provider } from "react-redux";
import "./App.css";
import "antd/dist/antd.css";
import Header from "./components/Header";
import { store } from "./store/store";
import { BrowserRouter, Switch, Route, Routes } from "react-router-dom";
import Settings from "./pages/SettingsUser";
import AutoSessions from "./pages/AutoSessions";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Header />
          <div style={{ width: "90%", margin: "10px 5%" }}>
            <Routes>
              <Route path={"/settings/user"} element={<Settings />}></Route>
              <Route path={"/"} element={<AutoSessions />} />
            </Routes>
          </div>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
